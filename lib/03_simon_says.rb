def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str,num=2)
  final_string = ""
  (0...num).each do |i|
    final_string << str
    final_string << " " unless i==num-1
  end
  final_string
end

def start_of_word(str,num)
  str[0...num]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  split_string = str.split
  title = ""
  split_string.each_index do |idx|
    title << split_string[idx].capitalize if idx == 0
    title << check_little(split_string[idx]) unless idx == 0
    title << " " unless idx+1 == split_string.length
  end
  title
end

def check_little(str)
  little_words = ["the","and","over","of"]
  return str if little_words.include?(str)
  str.capitalize
end
