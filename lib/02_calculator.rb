def add(num1,num2)
  num1+num2
end

def subtract(num1,num2)
  num1-num2
end

def sum(arr)
  arr << 0
  arr.reduce(:+)
end

def multiply(num1, num2)
  num1*num2
end

def power(num1,num2)
  num1**num2
end

def factorial(number)
  return 1 if number<=0
  (1..number).to_a.reduce(:*)
end
