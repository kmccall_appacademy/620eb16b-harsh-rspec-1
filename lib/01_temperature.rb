def ftoc(temp_f)
  factor = 5.to_f / 9.to_f
  (temp_f-32)*factor
end

def ctof(temp_c)
  factor = 9.to_f / 5.to_f
  temp_c*factor+32
end
