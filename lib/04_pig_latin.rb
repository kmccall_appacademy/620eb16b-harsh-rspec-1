

#Translates full sentence into pig latin
def translate(str)
  str.split.map { |word| piggify(word) }.join(" ")
end

def piggify(word)
  pig = word.downcase.chars
  if punctuated?(pig)
    punctuation = pig.last
    pig = pig[0...-1]
  end
  pig = move_consonants(pig)
  pig = pig.capitalize if word == word.capitalize
  pig = "#{pig}ay#{punctuation}"
end

def move_consonants(word)
  vowels = "aeiou"
  until vowels.include?(word.first)
    word.push(word.shift)
    word.push(word.shift) if word.last == 'q' && word.first == 'u'
  end
  word.join
end

def punctuated?(word)
  punctuation = '.,;:?!'
  punctuation.include?(word.last)
end
